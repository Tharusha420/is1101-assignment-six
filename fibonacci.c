#include<stdio.h>
int no1=0,no2=1,no3;
void Fibonacci(int n){

    if(n>0){
         no3 = no1 + no2;
         no1 = no2;
         no2 = no3;
         printf("%d ",no3);
         Fibonacci(n-1);
    }
}
int main(){
    int n;
    printf("Enter a number: ");
    scanf("%d",&n);
    printf("Fibonacci Series: ");
    printf("%d %d ",0,1);
    Fibonacci(n-2);//n-2 because 0 and 1 are already printed
  return 0;
 }
