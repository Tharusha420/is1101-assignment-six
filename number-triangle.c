#include <stdio.h>

void Row(int x);
void pattern(int NoOfRows);
int NoOfRows=1;
int x;

void Row(int x)
{

    if(x>0)
    {
        pattern(NoOfRows);
        printf("\n");
        NoOfRows++;
        Row(x-1);

    }
}


void pattern(int NoOfRows)
{
    if(NoOfRows>0)
    {
        printf("%d",NoOfRows);
        pattern(NoOfRows-1);
    }
}

int main()
{

    printf("Enter a number to generate number triangle: ");
    scanf("%d",&x);
    Row(x);
    return 0;
}
